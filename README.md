# rummy

A simple networked Rummikub-like game with a TUI.

## Rules differences

- no need to have 30 point to start
- no joker

## Cheating

With the protocol used, it is easy to cheat. Most check are done on the client
side. This implementation is intended to be played by player that trust each
others.

## License

Licensed under either of

 * Apache License, Version 2.0, ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

## Contribution

Unless you explicitly state otherwise, any contribution intentionally
submitted for inclusion in the work by you, as defined in the Apache-2.0
license, shall be dual licensed as above, without any additional terms or
conditions.
