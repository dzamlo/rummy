use std::collections::HashMap;
use std::ops::DerefMut;
use std::panic;
use std::sync::Mutex;

use anyhow::Result;
use common::api::game_server::{Game, GameServer};
use common::{api, Board, GameEvent, TileSequence, INITIAL_DRAW_LENGTH};
use rand::prelude::*;
use tokio::sync::{broadcast, mpsc};
use tokio_stream::wrappers::ReceiverStream;
use tonic::{transport::Server, Request, Response, Status};

#[derive(Debug)]
struct GameInstance {
    number_of_player: u32,
    current_player: u32,
    game_event_sender: broadcast::Sender<GameEvent>,
    pool: TileSequence,
}

impl GameInstance {
    fn next_turn(&mut self) {
        self.current_player = if self.current_player == self.number_of_player {
            1
        } else {
            self.current_player + 1
        };

        tracing::debug!(self.current_player, "new player turn");

        self.game_event_sender
            .send(GameEvent::PlayerTurn(self.current_player))
            .unwrap();
    }

    fn get_initial_draw(&mut self) -> TileSequence {
        let split_at = self.pool.len().saturating_sub(INITIAL_DRAW_LENGTH);
        let tiles = self.pool.tiles.split_off(split_at);
        tracing::debug!("{} tiles remaing after initial draw", self.pool.len());
        TileSequence { tiles }
    }
}

#[derive(Debug)]
pub struct GameService {
    games: Mutex<HashMap<u32, GameInstance>>,
    rng: Mutex<StdRng>,
}

impl GameService {
    fn map_game<T, F>(&self, game_id: u32, func: F) -> Result<T, Status>
    where
        F: FnOnce(&mut GameInstance) -> Result<T, Status>,
    {
        let mut games = self.games.lock().unwrap();

        if let Some(game) = games.get_mut(&game_id) {
            func(game)
        } else {
            tracing::debug!(%game_id, "game not found");
            Err(Status::not_found("no game with this game_id"))
        }
    }
}

impl Default for GameService {
    fn default() -> Self {
        let rng = Mutex::new(StdRng::from_entropy());
        let games = Mutex::new(HashMap::default());
        GameService { games, rng }
    }
}

pub async fn handle_game_events(
    game_id: u32,
    player_id: u32,
    initial_draw: TileSequence,
    mut game_event_receiver: broadcast::Receiver<GameEvent>,
    tx: mpsc::Sender<Result<api::GameEvent, Status>>,
) -> Result<()> {
    let event = GameEvent::GameId(game_id);
    tracing::debug!(?event, "sending game event");
    tx.send(Ok(event.into_api())).await.unwrap();

    let event = GameEvent::PlayerId(player_id);
    tracing::debug!(?event, "sending game event");
    tx.send(Ok(event.into_api())).await.unwrap();

    let event = GameEvent::InitialDraw(initial_draw);
    tracing::debug!(?event, "sending game event");
    tx.send(Ok(event.into_api())).await.unwrap();

    loop {
        let event = game_event_receiver.recv().await.unwrap();
        tracing::debug!(?event, "sending game event");
        if tx.send(Ok(event.into_api())).await.is_err() {
            tracing::debug!("connection closed by client");
            break;
        };

        if let GameEvent::PlayerWon(_) = event {
            break;
        }
    }
    Ok(())
}

#[tonic::async_trait]
impl Game for GameService {
    type JoinStream = ReceiverStream<Result<api::GameEvent, Status>>;
    #[tracing::instrument(skip(self))]
    async fn join(
        &self,
        request: Request<api::JoinRequest>,
    ) -> Result<Response<Self::JoinStream>, Status> {
        tracing::info!("join called");
        let mut game_id = request.get_ref().game_id;

        if game_id == 0 {
            tracing::debug!("joining with game_id == 0, creating a new game");
            let mut games = self.games.lock().unwrap();
            let mut new_game_id = 0;
            for i in 1..u32::MAX {
                if !games.contains_key(&i) {
                    new_game_id = i;
                    break;
                }
            }
            game_id = new_game_id;

            tracing::debug!(%new_game_id, "new game id found");

            let (game_event_sender, _) = broadcast::channel(16);
            let mut pool = TileSequence::all_tiles();

            let mut rng = self.rng.lock().unwrap();
            let rng = rng.deref_mut();
            pool.tiles.shuffle(rng);

            let new_game_instance = GameInstance {
                number_of_player: 0,
                current_player: 0,
                game_event_sender,
                pool,
            };
            games.insert(new_game_id, new_game_instance);
        }

        self.map_game(game_id, |game| {
            game.number_of_player += 1;
            let player_id = game.number_of_player;
            let _ = game.game_event_sender.send(GameEvent::NewPlayer(player_id));
            let game_event_receiver = game.game_event_sender.subscribe();
            let initial_draw = game.get_initial_draw();
            let (tx, rx) = mpsc::channel(1);

            tokio::spawn(async move {
                handle_game_events(game_id, player_id, initial_draw, game_event_receiver, tx)
                    .await
                    .unwrap();
            });
            Ok(Response::new(ReceiverStream::new(rx)))
        })
    }

    #[tracing::instrument(skip(self))]
    async fn start(
        &self,
        request: Request<api::StartRequest>,
    ) -> Result<Response<api::StartResponse>, Status> {
        tracing::info!("start called");
        let request_body = request.get_ref();
        let game_id = request_body.game_id;

        self.map_game(game_id, |game| {
            let mut rng = self.rng.lock().unwrap();
            let first_player = rng.gen_range(1..=game.number_of_player);
            game.current_player = first_player;
            game.game_event_sender
                .send(GameEvent::PlayerTurn(first_player))
                .unwrap();
            Ok(Response::new(api::StartResponse {}))
        })
    }

    #[tracing::instrument(skip(self))]
    async fn new_board(
        &self,
        request: Request<api::NewBoardRequest>,
    ) -> Result<Response<api::NewBoardResponse>, Status> {
        tracing::info!("new_board called");
        let request_body = request.get_ref();
        let game_id = request_body.game_id;
        let mut new_board = Board::from_api(&request_body.new_board);

        // Remove empty sequences
        new_board.sequences.retain(|s| !s.is_empty());

        self.map_game(game_id, |game| {
            game.game_event_sender
                .send(GameEvent::NewBoard(new_board))
                .unwrap();
            game.next_turn();
            Ok(Response::new(api::NewBoardResponse {}))
        })
    }

    #[tracing::instrument(skip(self))]
    async fn draw(
        &self,
        request: Request<api::DrawRequest>,
    ) -> Result<Response<api::DrawResponse>, Status> {
        tracing::info!("draw called");
        let request_body = request.get_ref();
        let game_id = request_body.game_id;

        self.map_game(game_id, |game| {
            if let Some(tile) = game.pool.tiles.pop() {
                tracing::debug!(?tile, "tile drawed");

                game.next_turn();

                Ok(Response::new(api::DrawResponse {
                    tile: Some(tile.into_api()),
                }))
            } else {
                tracing::debug!("the pool is empty");
                Err(Status::failed_precondition("the pool is empty"))
            }
        })
    }

    #[tracing::instrument(skip(self))]
    async fn won(
        &self,
        request: Request<api::WonRequest>,
    ) -> Result<Response<api::WonResponse>, Status> {
        tracing::info! {
            "won called"
        };
        let request_body = request.get_ref();
        let game_id = request_body.game_id;
        let player_id = request_body.player_id;
        self.map_game(game_id, |game| {
            let _ = game.game_event_sender.send(GameEvent::PlayerWon(player_id));
            Ok(Response::new(api::WonResponse {}))
        })
    }

    #[tracing::instrument(skip(self))]
    async fn ping(
        &self,
        _request: Request<api::PingRequest>,
    ) -> Result<Response<api::PingResponse>, Status> {
        tracing::info!("ping called");
        Ok(Response::new(api::PingResponse {}))
    }
}

#[tokio::main]
async fn main() -> Result<()> {
    panic::set_hook(Box::new(|panic_info| tracing::error!(%panic_info)));

    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::DEBUG)
        .init();

    let addr = "0.0.0.0:8080".parse()?;

    let game_service = GameService::default();
    let relfection_service = tonic_reflection::server::Builder::configure()
        .register_encoded_file_descriptor_set(api::FILE_DESCRIPTOR_SET)
        .build()?;

    tracing::info!(message = "Starting to listen", %addr);

    Server::builder()
        .trace_fn(|_| tracing::info_span!("rummy_server"))
        .add_service(relfection_service)
        .add_service(GameServer::new(game_service))
        .serve(addr)
        .await?;

    Ok(())
}
