FROM rust:latest as build

WORKDIR /rummy

# copy over your manifests
COPY ./Cargo.lock ./Cargo.lock
COPY ./Cargo.toml ./Cargo.toml
COPY common ./common
COPY client ./client
COPY server ./server

WORKDIR /rummy/server
RUN apt update -y
RUN apt install -y protobuf-compiler
RUN cargo build --release

FROM rust:latest

COPY --from=build /rummy/target/release/server .

CMD ["./server"]

