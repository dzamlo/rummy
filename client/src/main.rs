use std::cmp::min;
use std::io;
use std::io::Write;
use std::ops::Not;

use crate::cli::Args;
use anyhow::Result;
use clap::Parser;
use common::api::game_client::GameClient;
use common::api::game_event::GameEvent;
use common::api::{
    DrawRequest, JoinRequest, NewBoardRequest, PingRequest, StartRequest, WonRequest,
};
use common::Board;
use common::{Tile, TileColor, TileSequence};
use crossterm::event::{Event, EventStream, KeyCode, KeyEvent, KeyEventKind};
use crossterm::style::Attribute;
use crossterm::style::Color;
use crossterm::style::Print;
use crossterm::style::SetAttribute;
use crossterm::style::SetForegroundColor;
use crossterm::terminal;
use crossterm::terminal::{Clear, ClearType};
use crossterm::QueueableCommand;
use crossterm::{cursor, ExecutableCommand};
use tokio::sync::mpsc;
use tokio_stream::StreamExt;
mod cli;

enum MenuItem {
    Reset,
    Validate,
    Draw,
    SortRack,
}

impl From<usize> for MenuItem {
    fn from(value: usize) -> Self {
        match value {
            0 => MenuItem::Reset,
            1 => MenuItem::Validate,
            2 => MenuItem::Draw,
            _ => MenuItem::SortRack,
        }
    }
}

impl MenuItem {
    const STRS: [&str; 4] = ["reset", "validate", "draw", "sort rack"];
    const COUNT: usize = MenuItem::STRS.len();
}

fn tile_color_to_crossterm(color: TileColor) -> Color {
    use TileColor::*;
    match color {
        Black => Color::Reset,
        Blue => Color::DarkBlue,
        Orange => Color::DarkYellow,
        Red => Color::DarkRed,
    }
}

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
enum Selection {
    NotSelected,
    Selected,
}

impl Not for Selection {
    type Output = Selection;
    fn not(self) -> <Self as Not>::Output {
        match self {
            Selection::NotSelected => Selection::Selected,
            Selection::Selected => Selection::NotSelected,
        }
    }
}

fn display_tile<W: Write>(writer: &mut W, tile: Tile, selection: Option<Selection>) -> Result<()> {
    if let Some(selection) = selection {
        match selection {
            Selection::NotSelected => writer.queue(SetAttribute(Attribute::Underlined))?,
            Selection::Selected => writer.queue(SetAttribute(Attribute::Reverse))?,
        };
    }

    writer.queue(SetForegroundColor(tile_color_to_crossterm(tile.color)))?;
    writer.queue(Print(format!("{}", tile.value)))?;

    if let Some(selection) = selection {
        match selection {
            Selection::NotSelected => writer.queue(SetAttribute(Attribute::NoUnderline))?,
            Selection::Selected => writer.queue(SetAttribute(Attribute::NoReverse))?,
        };
    }
    Ok(())
}

#[derive(Copy, Clone, Debug)]
struct SequenceCursor {
    index: usize,
    selection: Selection,
}

fn display_tile_sequence<W: Write>(
    writer: &mut W,
    sequence: &TileSequence,
    cursor: Option<SequenceCursor>,
) -> Result<()> {
    if !sequence.is_empty() {
        let first_tile = sequence.tiles[0];
        display_tile(
            writer,
            first_tile,
            cursor.and_then(|c| {
                if c.index == 0 {
                    Some(c.selection)
                } else {
                    None
                }
            }),
        )?;
        for (i, tile) in sequence.tiles[1..].iter().enumerate() {
            writer.queue(Print(" "))?;
            display_tile(
                writer,
                *tile,
                cursor.and_then(|c| {
                    if c.index == i + 1 {
                        Some(c.selection)
                    } else {
                        None
                    }
                }),
            )?;
        }
    } else if cursor.is_some() {
        writer.queue(SetForegroundColor(Color::Reset))?;
        writer.queue(SetAttribute(Attribute::Underlined))?;
        writer.queue(Print(" "))?;
        writer.queue(SetAttribute(Attribute::NoUnderline))?;
    }
    Ok(())
}

fn display_menu<W: Write>(writer: &mut W, selected: Option<usize>, message: &str) -> Result<()> {
    writer.queue(SetForegroundColor(Color::Reset))?;
    for (i, text) in MenuItem::STRS.iter().enumerate() {
        if selected == Some(i) {
            writer.queue(SetAttribute(Attribute::Reverse))?;
        }
        writer.queue(Print(text))?;
        writer.queue(SetAttribute(Attribute::NoReverse))?;
        writer.queue(Print(" "))?;
    }
    writer.queue(Print(message))?;
    writer.queue(Print("\r\n"))?;
    Ok(())
}

#[derive(Copy, Clone, Debug)]
struct Cursor {
    index: usize,
    sequence_cursor: SequenceCursor,
}

fn display_board<W: Write>(writer: &mut W, board: &Board, cursor: Cursor) -> Result<()> {
    for (i, sequence) in board.sequences.iter().enumerate() {
        if i > 0 {
            writer.queue(Print("\n"))?;
        }
        writer.queue(Print("\r"))?;
        display_tile_sequence(
            writer,
            sequence,
            if cursor.index == i + 1 {
                Some(cursor.sequence_cursor)
            } else {
                None
            },
        )?;

        if i == 0 {
            // print the separation between the rack and the board
            writer.queue(SetForegroundColor(Color::Reset))?;
            writer.queue(Print("\r\n"))?;
            let (columns, _) = terminal::size()?;
            writer.queue(SetAttribute(Attribute::CrossedOut))?;
            for _ in 0..columns {
                writer.queue(Print(" "))?;
            }
            writer.queue(SetAttribute(Attribute::NotCrossedOut))?;
        }
    }
    writer.flush()?;
    Ok(())
}

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

impl Direction {
    fn from_key_code(key_code: KeyCode) -> Option<Direction> {
        match key_code {
            KeyCode::Up => Some(Direction::Up),
            KeyCode::Down => Some(Direction::Down),
            KeyCode::Left => Some(Direction::Left),
            KeyCode::Right => Some(Direction::Right),
            _ => None,
        }
    }
}

fn next_x(board: &Board, next_y: usize, x: usize) -> usize {
    if next_y == 0 {
        min(x, MenuItem::COUNT - 1)
    } else {
        let len = board.sequences[next_y - 1].len();
        let last_index = if len == 0 { 0 } else { len - 1 };
        min(x, last_index)
    }
}

fn move_cursor(current_cursor: Cursor, direction: Direction, board: &Board) -> Cursor {
    let y = current_cursor.index;
    let x = current_cursor.sequence_cursor.index;
    let selection = current_cursor.sequence_cursor.selection;

    let num_of_sequence = board.len() + 1;

    let sequence_len = if y == 0 {
        MenuItem::COUNT
    } else {
        board.sequences[y - 1].len()
    };

    let (y, x) = match direction {
        Direction::Up => {
            let next_y = if y == 0 { num_of_sequence - 1 } else { y - 1 };
            (next_y, next_x(board, next_y, x))
        }
        Direction::Down => {
            let next_y = (y + 1) % num_of_sequence;
            (next_y, next_x(board, next_y, x))
        }
        Direction::Left => {
            if sequence_len > 0 {
                (y, if x == 0 { sequence_len - 1 } else { x - 1 })
            } else {
                (y, x)
            }
        }
        Direction::Right => {
            if sequence_len > 0 {
                (y, (x + 1) % sequence_len)
            } else {
                (y, x)
            }
        }
    };

    Cursor {
        index: y,
        sequence_cursor: SequenceCursor {
            index: x,
            selection,
        },
    }
}

fn move_tile(current_cursor: &mut Cursor, direction: Direction, board: &mut Board) {
    let y = current_cursor.index - 1;
    let x = current_cursor.sequence_cursor.index;

    match direction {
        Direction::Right | Direction::Left => {
            let sequence = &mut board.sequences[y];
            let swap_index = if direction == Direction::Right {
                (x + 1) % sequence.len()
            } else if x == 0 {
                sequence.len() - 1
            } else {
                x - 1
            };
            let tile = sequence.tiles.remove(x);
            sequence.tiles.insert(swap_index, tile);
            current_cursor.sequence_cursor.index = swap_index;
        }
        Direction::Down => {
            if y == board.len() - 1 {
                if board.sequences[y].len() == 1 {
                    return;
                }
                board.sequences.push(vec![].into());
            }
            let tile = board.sequences[y].tiles.remove(x);
            let insert_seq = &mut board.sequences[y + 1];
            let insert_index = min(insert_seq.len(), x);
            insert_seq.tiles.insert(insert_index, tile);
            current_cursor.index += 1;
            current_cursor.sequence_cursor.index = insert_index;
        }
        Direction::Up => {
            let tile = board.sequences[y].tiles.remove(x);
            // if y is 1 we want to move to the last line and not in the rack
            let insert_seq_index = if y <= 1 { board.len() - 1 } else { y - 1 };
            let insert_seq = &mut board.sequences[insert_seq_index];
            let insert_index = min(insert_seq.len(), x);
            insert_seq.tiles.insert(insert_index, tile);
            current_cursor.index = insert_seq_index + 1;
            current_cursor.sequence_cursor.index = insert_index;
            if board.sequences.last().unwrap().is_empty() {
                board.sequences.pop();
            }
        }
    }
}

#[derive(Debug)]
enum GameInputEvent {
    GameEvent(GameEvent),
    KeyPress(KeyCode),
    Err(anyhow::Error),
    PingRequest,
}

#[derive(Debug, Default)]
struct GameInstance {
    game_id: u32,
    player_id: u32,
    board: Board,
    previous_board: Board,
    current_player_turn: u32,
}

impl GameInstance {
    fn is_current_player_turn(&self) -> bool {
        self.player_id == self.current_player_turn
    }

    fn sort_rack(&mut self) {
        let initial_rack = &self.board.sequences[0];
        let previous_rack = &self.previous_board.sequences[0];
        let nothing_played = previous_rack == initial_rack;

        let mut rack = initial_rack.clone();
        rack.sort_color_first();
        if rack == *initial_rack {
            rack.sort_value_first();
        }
        if nothing_played {
            self.previous_board.sequences[0] = rack.clone();
        }
        self.board.sequences[0] = rack;
    }
}

async fn game_loop<W: Write>(
    writer: &mut W,
    rx: &mut mpsc::Receiver<GameInputEvent>,
    game_instance: &mut GameInstance,
    client: &mut GameClient<tonic::transport::Channel>,
) -> Result<()> {
    writer.queue(terminal::EnterAlternateScreen)?;
    writer.execute(cursor::Hide)?;
    terminal::enable_raw_mode()?;

    let mut cursor = Cursor {
        index: 0,
        sequence_cursor: SequenceCursor {
            index: 0,
            selection: Selection::NotSelected,
        },
    };

    let mut winner = None;

    let mut message: String = if game_instance.is_current_player_turn() {
        "It's your turn.".into()
    } else {
        "".into()
    };

    loop {
        writer.queue(Clear(ClearType::All))?;
        writer.queue(cursor::MoveTo(0, 0))?;
        display_menu(
            writer,
            if cursor.index == 0 {
                Some(cursor.sequence_cursor.index)
            } else {
                None
            },
            &message,
        )?;
        display_board(writer, &game_instance.board, cursor)?;
        writer.flush()?;

        if let Some(e) = rx.recv().await {
            match e {
                GameInputEvent::KeyPress(KeyCode::Char('q')) => break,

                GameInputEvent::KeyPress(KeyCode::Char(' ') | KeyCode::Enter) => {
                    if cursor.index > 0
                        && !game_instance.board.sequences[cursor.index - 1]
                            .tiles
                            .is_empty()
                    {
                        cursor.sequence_cursor.selection = !cursor.sequence_cursor.selection;
                    } else if cursor.index == 0 && game_instance.is_current_player_turn() {
                        match cursor.sequence_cursor.index.into() {
                            MenuItem::Reset => {
                                game_instance.board = game_instance.previous_board.clone();
                            }
                            MenuItem::Validate => {
                                let all_is_valid = game_instance.board.sequences[1..]
                                    .iter()
                                    .all(|s| s.is_empty() || s.is_valid());
                                let played_at_least_one = game_instance.board.sequences[0].len()
                                    < game_instance.previous_board.sequences[0].len();

                                if all_is_valid && played_at_least_one {
                                    if game_instance.board.sequences[0].is_empty() {
                                        client
                                            .won(WonRequest {
                                                game_id: game_instance.game_id,
                                                player_id: game_instance.player_id,
                                            })
                                            .await?;
                                    } else {
                                        client
                                            .new_board(NewBoardRequest {
                                                game_id: game_instance.game_id,
                                                new_board: Some(
                                                    game_instance.board.into_api_without_first(),
                                                ),
                                            })
                                            .await?;
                                    }
                                } else if !played_at_least_one {
                                    message = "You need to play at least one tile.".into();
                                } else {
                                    message = "Your sequences are invalid.".into();
                                }
                            }
                            MenuItem::Draw => {
                                if game_instance.board == game_instance.previous_board {
                                    let response = client
                                        .draw(DrawRequest {
                                            game_id: game_instance.game_id,
                                        })
                                        .await;
                                    match response {
                                        Ok(response) => {
                                            let tile: Tile = response
                                                .get_ref()
                                                .tile
                                                .clone()
                                                .map(|t| t.into())
                                                .unwrap_or_default();
                                            game_instance.board.sequences[0].tiles.push(tile)
                                        }
                                        Err(e) => {
                                            message = format!(
                                                "an error occured while drawing a tile: {}",
                                                e
                                            )
                                        }
                                    }
                                } else {
                                    message = "You can only draw when you didn't do anything. Please use the reset button.".into()
                                }
                            }
                            MenuItem::SortRack => game_instance.sort_rack(),
                        }
                    }
                }

                GameInputEvent::KeyPress(
                    key_code @ (KeyCode::Up | KeyCode::Down | KeyCode::Left | KeyCode::Right),
                ) => {
                    let direction = Direction::from_key_code(key_code).unwrap();
                    if cursor.sequence_cursor.selection == Selection::Selected {
                        if game_instance.is_current_player_turn() {
                            move_tile(&mut cursor, direction, &mut game_instance.board);
                        }
                    } else {
                        cursor = move_cursor(cursor, direction, &game_instance.board);
                    }
                }

                GameInputEvent::GameEvent(GameEvent::NewBoard(board)) => {
                    let board = Board::from_api(&Some(board));
                    game_instance.board.sequences.truncate(1);
                    game_instance.board.sequences.extend(board.sequences);
                }
                GameInputEvent::GameEvent(GameEvent::PlayerTurn(id)) => {
                    game_instance.current_player_turn = id;
                    if game_instance.is_current_player_turn() {
                        message = format!("It's your turn.");
                        game_instance.previous_board = game_instance.board.clone();
                    } else {
                        message = format!("It's player {} turn.", id);
                    }
                }
                GameInputEvent::GameEvent(GameEvent::PlayerWon(id)) => {
                    winner = Some(id);
                    break;
                }
                GameInputEvent::Err(e) => message = format!("An error happened: {}.", e),
                GameInputEvent::PingRequest => {
                    client.ping(PingRequest {}).await?;
                }
                _ => (),
            }
        } else {
            break;
        }
    }

    writer.queue(terminal::LeaveAlternateScreen)?;
    writer.execute(cursor::Show)?;
    terminal::disable_raw_mode()?;

    if let Some(id) = winner {
        writer.execute(Print(format!("Player {} won!\n", id)))?;
    }

    Ok(())
}

#[tokio::main]
async fn main() -> Result<()> {
    let args = Args::parse();

    let mut client = GameClient::connect(args.server_url).await?;
    let mut game_instance = GameInstance {
        game_id: args.game_id,
        ..Default::default()
    };

    let mut game_events = client
        .join(JoinRequest {
            game_id: args.game_id,
        })
        .await?
        .into_inner();

    let (tx, mut rx) = mpsc::channel::<GameInputEvent>(16);
    let tx2 = tx.clone();
    let tx3 = tx.clone();

    tokio::spawn(async move {
        loop {
            let response = game_events.message().await;
            let event = match response {
                Err(e) => GameInputEvent::Err(e.into()),
                Ok(Some(e)) => GameInputEvent::GameEvent(e.game_event.unwrap()),
                Ok(None) => break,
            };

            tx.send(event).await.unwrap()
        }
    });

    tokio::spawn(async move {
        let mut event_stream = EventStream::new();
        while let Some(event) = event_stream.next().await {
            if let Ok(Event::Key(KeyEvent {
                code,
                kind: KeyEventKind::Press | KeyEventKind::Repeat,
                ..
            })) = event
            {
                tx2.send(GameInputEvent::KeyPress(code)).await.unwrap()
            }
        }
    });

    tokio::spawn(async move {
        let mut interval = tokio::time::interval(tokio::time::Duration::from_secs(10));
        loop {
            interval.tick().await;
            tx3.send(GameInputEvent::PingRequest).await.unwrap();
        }
    });

    let mut stdout = io::stdout();
    stdout.execute(Print("Press enter to start the game.\n"))?;

    while let Some(e) = rx.recv().await {
        match e {
            GameInputEvent::KeyPress(KeyCode::Enter) => {
                client
                    .start(StartRequest {
                        game_id: game_instance.game_id,
                    })
                    .await?;
            }
            GameInputEvent::GameEvent(GameEvent::PlayerTurn(id)) => {
                game_instance.current_player_turn = id;
                game_loop(&mut stdout, &mut rx, &mut game_instance, &mut client).await?;
                break;
            }
            GameInputEvent::GameEvent(GameEvent::InitialDraw(initial_draw)) => {
                game_instance.board.sequences.push(initial_draw.into());
                game_instance.previous_board = game_instance.board.clone();
            }
            GameInputEvent::GameEvent(GameEvent::GameId(id)) => {
                println!("Game id {}.", id);
                game_instance.game_id = id;
            }
            GameInputEvent::GameEvent(GameEvent::PlayerId(id)) => {
                println!("You are player {}.", id);
                game_instance.player_id = id;
            }
            GameInputEvent::GameEvent(GameEvent::NewPlayer(id)) => {
                println!("Player {} joined the game.", id);
            }
            GameInputEvent::PingRequest => {
                client.ping(PingRequest {}).await?;
            }
            _ => (),
        }
    }

    Ok(())
}
