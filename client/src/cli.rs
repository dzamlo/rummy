use clap::Parser;

/// A rummikub-like game with a TUI
#[derive(Parser, Debug)]
#[command(author, version, about)]
pub struct Args {
    /// URL of the server to connect to
    #[arg(short, long, default_value = "https://rummy.fly.dev")]
    pub server_url: String,

    /// game id to join, 0 to create a new game
    #[arg(short, long, default_value_t = 0)]
    pub game_id: u32,
}
