use std::collections::HashSet;

pub mod api {
    tonic::include_proto!("api");

    pub const FILE_DESCRIPTOR_SET: &[u8] = tonic::include_file_descriptor_set!("api_descriptor");
}

pub const INITIAL_DRAW_LENGTH: usize = 14;
pub const MIN_TILE_VALUE: u8 = 1;
pub const MAX_TILE_VALUE: u8 = 13;
pub const NUMBER_OF_COPY_OF_EACH_TILE: usize = 2;

#[derive(Copy, Clone, Debug, Default, Eq, Hash, PartialEq, Ord, PartialOrd)]
pub enum TileColor {
    #[default]
    Black,
    Blue,
    Orange,
    Red,
}

impl TileColor {
    pub fn all() -> [TileColor; 4] {
        use TileColor::*;
        [Black, Blue, Orange, Red]
    }

    pub fn into_api(&self) -> api::TileColor {
        use api::TileColor as T;
        match *self {
            TileColor::Black => T::Black,
            TileColor::Blue => T::Blue,
            TileColor::Orange => T::Orange,
            TileColor::Red => T::Red,
        }
    }
}

impl From<api::TileColor> for TileColor {
    fn from(value: api::TileColor) -> Self {
        use api::TileColor as T;
        match value {
            T::Black => TileColor::Black,
            T::Blue => TileColor::Blue,
            T::Orange => TileColor::Orange,
            T::Red => TileColor::Red,
        }
    }
}

impl From<i32> for TileColor {
    fn from(value: i32) -> Self {
        use api::TileColor as T;
        match value {
            n if n == T::Black as i32 => TileColor::Black,
            n if n == T::Blue as i32 => TileColor::Blue,
            n if n == T::Orange as i32 => TileColor::Orange,
            n if n == T::Red as i32 => TileColor::Red,
            _ => unreachable!(),
        }
    }
}

#[derive(Copy, Clone, Debug, Default, Eq, Hash, PartialEq)]
pub struct Tile {
    pub color: TileColor,
    pub value: u8,
}

impl Tile {
    pub fn new(color: TileColor, value: u8) -> Tile {
        Tile { color, value }
    }

    pub fn into_api(&self) -> api::Tile {
        api::Tile {
            color: self.color.into_api() as i32,
            value: self.value as u32,
        }
    }
}

impl From<api::Tile> for Tile {
    fn from(value: api::Tile) -> Self {
        Tile {
            color: value.color.into(),
            value: value.value as u8,
        }
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct TileSequence {
    pub tiles: Vec<Tile>,
}

impl TileSequence {
    pub fn all_tiles() -> Self {
        let mut tiles = Vec::with_capacity(
            (MAX_TILE_VALUE - MIN_TILE_VALUE + 1) as usize
                * TileColor::all().len()
                * NUMBER_OF_COPY_OF_EACH_TILE,
        );

        for color in TileColor::all() {
            for i in MIN_TILE_VALUE..=MAX_TILE_VALUE {
                for _ in 0..NUMBER_OF_COPY_OF_EACH_TILE {
                    tiles.push(Tile::new(color, i))
                }
            }
        }

        TileSequence { tiles }
    }

    pub fn into_api(&self) -> api::Sequence {
        api::Sequence {
            tiles: self.tiles.iter().map(|t| t.into_api()).collect(),
        }
    }

    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    pub fn is_valid(&self) -> bool {
        if self.tiles.len() < 3 {
            return false;
        }
        self.is_set() || self.is_suite()
    }

    fn is_set(&self) -> bool {
        let all_values_equal = self.tiles.windows(2).all(|v| v[0].value == v[1].value);
        if all_values_equal {
            let mut colors = HashSet::with_capacity(self.tiles.len());
            for tile in &self.tiles {
                colors.insert(tile.color);
            }
            colors.len() == self.tiles.len()
        } else {
            false
        }
    }

    fn is_suite(&self) -> bool {
        let all_colors_equal = self.tiles.windows(2).all(|v| v[0].color == v[1].color);
        let values_are_suite = self
            .tiles
            .windows(2)
            // we convert to al larger type to avoid overflow
            .all(|v| v[0].value as u16 + 1 == v[1].value as u16);
        all_colors_equal && values_are_suite
    }

    pub fn len(&self) -> usize {
        self.tiles.len()
    }

    pub fn sort_color_first(&mut self) {
        self.tiles
            .sort_by(|t1, t2| t1.color.cmp(&t2.color).then(t1.value.cmp(&t2.value)));
    }

    pub fn sort_value_first(&mut self) {
        self.tiles
            .sort_by(|t1, t2| t1.value.cmp(&t2.value).then(t1.color.cmp(&t2.color)));
    }
}

impl From<Vec<Tile>> for TileSequence {
    fn from(value: Vec<Tile>) -> Self {
        TileSequence { tiles: value }
    }
}

impl From<api::Sequence> for TileSequence {
    fn from(value: api::Sequence) -> Self {
        let tiles = value.tiles.into_iter().map(|t| t.into()).collect();
        TileSequence { tiles }
    }
}

#[derive(Clone, Debug, Default, PartialEq, Eq)]
pub struct Board {
    pub sequences: Vec<TileSequence>,
}

impl Board {
    pub fn from_api(b: &Option<api::Board>) -> Self {
        let sequences = if let Some(b) = b {
            b.sequences.clone().into_iter().map(|s| s.into()).collect()
        } else {
            vec![]
        };

        Board { sequences }
    }

    pub fn into_api(&self) -> api::Board {
        api::Board {
            sequences: self.sequences.iter().map(|s| s.into_api()).collect(),
        }
    }

    pub fn into_api_without_first(&self) -> api::Board {
        api::Board {
            sequences: self
                .sequences
                .iter()
                .skip(1)
                .map(|s| s.into_api())
                .collect(),
        }
    }

    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    pub fn len(&self) -> usize {
        self.sequences.len()
    }
}

#[derive(Clone, Debug)]
pub enum GameEvent {
    GameId(u32),
    PlayerId(u32),
    NewPlayer(u32),
    InitialDraw(TileSequence),
    NewBoard(Board),
    PlayerTurn(u32),
    PlayerWon(u32),
}

impl GameEvent {
    pub fn into_api(&self) -> api::GameEvent {
        use api::game_event::GameEvent as G;
        let game_event = match self {
            GameEvent::GameId(id) => G::GameId(*id),
            GameEvent::PlayerId(id) => G::PlayerId(*id),
            GameEvent::NewPlayer(id) => G::NewPlayer(*id),
            GameEvent::InitialDraw(sequence) => G::InitialDraw(sequence.into_api()),
            GameEvent::NewBoard(board) => G::NewBoard(board.into_api()),
            GameEvent::PlayerTurn(id) => G::PlayerTurn(*id),
            GameEvent::PlayerWon(id) => G::PlayerWon(*id),
        };
        api::GameEvent {
            game_event: Some(game_event),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_tile_sequence_is_valid() {
        use TileColor::*;

        let empty: TileSequence = vec![].into();
        assert!(!empty.is_valid());

        for color in TileColor::all() {
            let len1: TileSequence = vec![Tile::new(color, 1)].into();
            assert!(!len1.is_valid());

            for color2 in TileColor::all() {
                let len2: TileSequence = vec![Tile::new(color, 1), Tile::new(color2, 2)].into();
                assert!(!len2.is_valid());
            }
        }

        let len3_suite: TileSequence = vec![
            Tile::new(Black, 1),
            Tile::new(Black, 2),
            Tile::new(Black, 3),
        ]
        .into();
        assert!(len3_suite.is_valid());

        let len3_suite_not_same_color: TileSequence = vec![
            Tile::new(Black, 1),
            Tile::new(Black, 2),
            Tile::new(Orange, 3),
        ]
        .into();
        assert!(!len3_suite_not_same_color.is_valid());

        let len3_set: TileSequence = vec![
            Tile::new(Black, 1),
            Tile::new(Blue, 1),
            Tile::new(Orange, 1),
        ]
        .into();
        assert!(len3_set.is_valid());

        let len3_set_same_color: TileSequence = vec![
            Tile::new(Black, 1),
            Tile::new(Orange, 1),
            Tile::new(Black, 1),
        ]
        .into();
        assert!(!len3_set_same_color.is_valid());

        let len3_set_not_same_value: TileSequence = vec![
            Tile::new(Black, 1),
            Tile::new(Blue, 1),
            Tile::new(Orange, 2),
        ]
        .into();
        assert!(!len3_set_not_same_value.is_valid());

        let len4_suite: TileSequence = vec![
            Tile::new(Black, 1),
            Tile::new(Black, 2),
            Tile::new(Black, 3),
            Tile::new(Black, 4),
        ]
        .into();
        assert!(len4_suite.is_valid());

        let len4_suite_not_same_color: TileSequence = vec![
            Tile::new(Black, 1),
            Tile::new(Black, 2),
            Tile::new(Black, 3),
            Tile::new(Orange, 4),
        ]
        .into();
        assert!(!len4_suite_not_same_color.is_valid());

        let len4_set: TileSequence = vec![
            Tile::new(Black, 1),
            Tile::new(Blue, 1),
            Tile::new(Orange, 1),
            Tile::new(Red, 1),
        ]
        .into();
        assert!(len4_set.is_valid());

        let len4_set_same_color: TileSequence = vec![
            Tile::new(Black, 1),
            Tile::new(Orange, 1),
            Tile::new(Red, 1),
            Tile::new(Black, 1),
        ]
        .into();
        assert!(!len4_set_same_color.is_valid());

        let len4_set_not_same_value: TileSequence = vec![
            Tile::new(Black, 1),
            Tile::new(Blue, 1),
            Tile::new(Orange, 1),
            Tile::new(Red, 2),
        ]
        .into();
        assert!(!len4_set_not_same_value.is_valid());

        let len5_suite: TileSequence = vec![
            Tile::new(Black, 1),
            Tile::new(Black, 2),
            Tile::new(Black, 3),
            Tile::new(Black, 4),
            Tile::new(Black, 5),
        ]
        .into();
        assert!(len5_suite.is_valid());

        let len5_suite_wrong_order: TileSequence = vec![
            Tile::new(Black, 2),
            Tile::new(Black, 3),
            Tile::new(Black, 4),
            Tile::new(Black, 5),
            Tile::new(Black, 1),
        ]
        .into();
        assert!(!len5_suite_wrong_order.is_valid());

        let len5_set: TileSequence = vec![
            Tile::new(Black, 1),
            Tile::new(Blue, 1),
            Tile::new(Orange, 1),
            Tile::new(Blue, 1),
            Tile::new(Red, 1),
        ]
        .into();
        assert!(!len5_set.is_valid());
    }
}
